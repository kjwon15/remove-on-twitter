// ==UserScript==
// @name remove ㅋ on twitter
// @namespace Kjwon15
// @description remove ㅋ on twitter
// @include http://twitter.com/*
// @include https://twitter.com/*
// ==/UserScript==

removeFunc = function(event){
    var pattern = /ㅋ/g;
    var tweets = document.querySelectorAll("p.tweet-text");
    for(var i=0; i<tweets.length; i++){
        tweets[i].innerHTML = tweets[i].innerHTML.replace(pattern, '');
    };
    event.preventDefault();
    return false;
}

window.addEventListener('load', removeFunc, false);

var li = document.createElement('li');
var button = document.createElement('a');
button.id = 'kuchen';
button.href = "/";
button.innerHTML = '<span class="text">remove</span>';
button.className = "js-nav";
button.onclick = removeFunc;
li.appendChild(button);
document.querySelector('ul#global-actions').appendChild(li);

